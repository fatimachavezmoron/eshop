import React, { useState } from 'react'
import Modal from '../Components/Modal/Modal'
import ModalProducts from '../Components/Modal/ModalProducts'
import './ProductDetail.css'
import { EOS_STAR, EOS_LOCAL_SHIPPING, EOS_EVENT_NOTE } from 'eos-icons-react'

const ProductDetail = () => {
  const [showModalProducts, setShowModalProducts] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const handleAddToCart = () => {
    setShowModal(true);
  };
  const handleShowProducts = () => {
    setShowModalProducts(true);
  }
  
  return (
    <>
      <div className='ContainerForChoose'>  
          <div className='block-price'>
            <h1 className='title_product_second'>Airpods-Max</h1>
            <p className="description">
              A perfect balance of exhilarating high-fidelity <br/>
              audio and the effortless magic of Airpods.
            </p>
          </div>
          <div className='blockStar'>
            <span className='star-container'>
              <EOS_STAR  className="eos_star" />
              <EOS_STAR  className="eos_star" />
              <EOS_STAR  className="eos_star" />
              <EOS_STAR  className="eos_star" />
              <EOS_STAR  className="eos_star" />
            </span>
            <p className="description">Suggested payments with 6 months <br/>
            special financing.
            </p>
          </div>
          <div className='ChooseColor'>
              <div className="colors">
                <button className='btn-chooseColor' onClick={handleShowProducts}>Choose a color</button>
                {showModalProducts && <ModalProducts showModalProducts={showModalProducts} 
                setShowModalProducts={setShowModalProducts}
                />}
              </div>
          </div>
          <div className='btn-Container'>
            <button className='btn-buy productItem'>Buy Now</button>
            <button className='btn-buy productItem' onClick={handleAddToCart}>
            Add to Cart</button>
            {showModal && <Modal showModal={showModal} setShowModal={setShowModal}
            />}
          </div>
          <div class="delivery">
            <div class="free">
              <EOS_LOCAL_SHIPPING  className="eos_delivery car" />
                <span class="fdelivery">Free Delivery
                </span>
                <p class="delivery-availability">
                  Enter yuor postal code for Delivery Availability
                </p>
            </div>
            <hr/>
            <div class="free">
              <EOS_EVENT_NOTE  className="eos_delivery" />
                <span class="fdelivery">Return Delivery
                </span>
                <p class="delivery-returns">
                  Free 30days Delivery Returns.<span class="underline"> Details</span>
                </p>
            </div>
          </div>
      </div>
    </>
  )
}

export default ProductDetail