import React from 'react'
import { useParams } from 'react-router-dom'
import { Link } from 'react-router-dom';
import products from '../../src/store/product.store.json'
import './LinkProducts.css'
import ProductDetail from './ProductDetail'
import headphone2 from '../images/headphone2.png';
import headphone11 from '../images/headphone11.png';
import headphone4 from '../images/headphone4.png';
import headphone8 from '../images/headphone8.png';

const LinkProduct = (item) => {
  const { index } = useParams();
  const product = products[index];

  return (
    <>
      <div className='Product-Link-Container'>
        <div className='Product-container'>
          <div className='first-gradient' />
          <img src={require(`../images/${product.image}`)} alt="Product" 
          className='Product-img' />
          <h2 className='title_product'>{product.type}</h2>
          <h1>{product.price}</h1>
          <p>{product.material}</p>
          <div className="Stock-Images">
            <Link to={`${process.env.PUBLIC_URL}/product/6`} >
            <img src={headphone11} alt='headphone_img' className='headphone_img'/>
            </Link>
            <Link to={`${process.env.PUBLIC_URL}/product/11`} >
            <img src={headphone4} alt='headphone_img' className='headphone_img'/>
            </Link>    
            <Link to={`${process.env.PUBLIC_URL}/product/3`} >
            <img src={headphone8} alt='headphone_img' className='headphone_img'/>
            </Link>
            <Link to={`${process.env.PUBLIC_URL}/product/9`} >
            <img src={headphone2} alt='headphone_img' className='headphone_img'/>
            </Link>
          </div>
        </div>
        <ProductDetail />
      </div>
    </>
  )
}

export default LinkProduct

