import React from 'react'
import './LoginGoogle.css'
import GoogleLogin from '@leecheuk/react-google-login';

function LoginGoogle () {
  const responseGoogle = (response) => {
    console.log(response);
  }
  return (
    <div className='LoginGoogle'>
        <GoogleLogin
    clientId="832201935059-k73iq96508pl82mjqh15fg32ehb7dcqf.apps.googleusercontent.com"
    buttonText="Login"
    onSuccess={responseGoogle}
    onFailure={responseGoogle}
    cookiePolicy={'single_host_origin'}
  />
    </div>
  );
}

export default LoginGoogle

