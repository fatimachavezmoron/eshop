import { createContext, useReducer } from 'react'
import productsDB from '../store/product.store.json'

export const ProductsDataContext = createContext(null);
export const ProductsDataDispatchContext = createContext(null);

const ProductsDataProvider = ({ children }) => {
  
  const PaginaState = {
    products: productsDB
  }

  const [products, dispatch] = useReducer(
    productsReducer,
    PaginaState
  );

  function productsReducer (state, action) {
    switch (action.type) {
      case 'FILTER_STARS': {
        console.log('State value', state)
        return {...state,
          products: productsDB.filter(item => item.stars === action.value) 
        }
      }
      case 'FILTER_COLOR': {
        return {...state,
          products: productsDB.filter(item => item.color === action.value) 
        }
      }
      case 'FILTER_MATERIAL': {
        return {...state,
          products: productsDB.filter(item => item.material === action.value) 
        }
      }
      case 'FILTER_PRICE': {
        return {...state,
          products: productsDB.filter(item => item.price === action.value) 
        }
      }
      default: {
        throw Error('Unknown action: ' + action.type);
      }
    }
  }

  return (
    <ProductsDataContext.Provider value={products}>
      <ProductsDataDispatchContext.Provider value={dispatch}>
        {children}
      </ProductsDataDispatchContext.Provider>
    </ProductsDataContext.Provider>
  )
}


export { ProductsDataProvider }
