import React from 'react';
import { BrowserRouter, Routes, Route  } from 'react-router-dom';
import NavBar from './Components/NavBar/index';
import OfferCont from './Components/contenedor/offer-container/index';
import ProductsComponent from './Components/products/index';
import ProductPrice from './Components/contenedor/productPrice/index';
import Register from './Login/Register';
import LinkProduct from './Pages/LinkProduct';
import { ProductsDataProvider } from './context/ProductsContext';
import Footer from './Components/footer/Footer';
import './assets/styles/index.scss';
import ScrollToTop from './ScrollToTop';
import Paypal from './Pages/Paypal';
import { PayPalScriptProvider} from "@paypal/react-paypal-js";



const App = () => {
  const initialOptions = {
    clientId: "AanZnSrJ4JeI2AMWE_5u2y9cz53-NjY9K1CodSKQmO9MU-mXbZkrgJ6Gvd44sgleC36x6DMB39ZvAzpY",
    currency: "USD",
    intent: "capture",
};
  return (
  <BrowserRouter>
  <PayPalScriptProvider options={initialOptions}>
  <ScrollToTop />
    <ProductsDataProvider>
      <NavBar />
      <Routes>
      <Route path={`${process.env.PUBLIC_URL}/`} element={<> 
            <OfferCont />
            <ProductsComponent />
            <ProductPrice />
          </>} />
      <Route path={`${process.env.PUBLIC_URL}/register`} element={<Register />} /> 
      <Route path={`${process.env.PUBLIC_URL}/product/:index`} element={<LinkProduct />} />
      <Route path={`${process.env.PUBLIC_URL}/paypal`} element={<Paypal />} />
      </Routes>
      <Footer />
    </ProductsDataProvider>
    </PayPalScriptProvider>
  </BrowserRouter>
  );
};

export default App;


