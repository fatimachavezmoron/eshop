import React from 'react'
import 'boxicons'



const Footer = () => {
  return (
    <>
    <div className='footer-container'>
      <h3 className='footer-copyright'>© Copyright 2023</h3>
    </div>
    <div>
    <ul class="footer-links">
            <li><a href="https://m.facebook.com/login/?locale=es_ES" 
            target="_blank" className="footer-link-icon" rel="noopener noreferrer">
            <box-icon type='logo' name='facebook-circle'></box-icon> </a>
            </li>
            <li><a href="https://twitter.com/home" 
            target="_blank" className="footer-link-icon" rel="noopener noreferrer">
            <box-icon type='logo' name='twitter'></box-icon></a>
            </li>
            <li><a href="https://www.instagram.com/" 
            target="_blank" className="footer-link-icon" rel="noopener noreferrer">
            <box-icon name='instagram-alt' type='logo' ></box-icon></a>
            </li>
          </ul>

    </div>
    </>
  )
}

export default Footer
