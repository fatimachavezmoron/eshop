import React, { useState, useContext } from 'react';
import { EOS_STAR } from 'eos-icons-react';
import { ProductsDataContext } from '../../../context/ProductsContext';
import { Link } from 'react-router-dom';
import Modal from '../../Modal/Modal';

const ProductPrice = () => {
  const [selectedProductIndex, setSelectedProductIndex] = useState(null);
  const [showModal, setShowModal] = useState(false);
  const productsData = useContext(ProductsDataContext);
  const [stocks, setStocks] = useState([1, 2, 1, 3, 1, 5, 1, 2, 3, 2, 1, 3]);

const AddToCart = (index) => {
  if (stocks[index] > 0) {
    const updatedStocks = [...stocks];
    updatedStocks[index] -= 1;
    setStocks(updatedStocks);
    setSelectedProductIndex(index); 
    setShowModal(true); 
  }
};


  return (
    <>
      <h2>Headphones For You!</h2>
      <div className="product-price">
        {productsData.products.map((item, index) => (
          <div key={index}>
            <Link to={`${process.env.PUBLIC_URL}/product/${index}`} target="_blank" rel="noopener noreferrer">
            <img src={require(`../../../images/${item.image}`)} 
            alt="Product" className="headpho-img" />
            </Link>
            <div className="HeadphoneText-cont">
              <p>
                <span className="type-hp">{item.type}</span>
                <span className="js_price-tag">{item.price}</span>
              </p>
              <p>{item.description}</p>
              <div className="stars-container">
                {[...Array(item.stars)].map((_, i) => (
                  <EOS_STAR key={i} className="eos_star" />
                ))}
              </div>
              <br />
              <button
                id={`js-btn-${index}`}
                className="btn-add-cart"
                disabled={stocks[index] === 0}
                onClick={() => AddToCart(index)}
              >
                {stocks[index] > 0 ? 'Add to Cart' : 'Sold Out'}
              </button>
            </div>
          </div>
        ))}
         <div className='third-gradient'/>
      </div>
      {showModal &&   <Modal
          showModal={showModal}
          setShowModal={setShowModal}
          product={productsData.products[selectedProductIndex]} />
      }
    </>
  );
};



export default ProductPrice;

