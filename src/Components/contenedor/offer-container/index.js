import girl from '../../../images/girl.png'

const OfferCont = () => (
  <div className="offer-container">
  <div className="offer">
    <h1 className="text-offer">Grab Upto <span className='span-offer'>50% Off</span> on
    Selected Headphone</h1>
    <button className="btn-buy">Buy Now </button>
  </div>
  <div class="block-photo">
    <img src={girl} className="photo-girl" alt='logo' />
        <div className='first-gradient'/>
        <div className='second-gradient'/>
  </div>
</div>
)

export default OfferCont