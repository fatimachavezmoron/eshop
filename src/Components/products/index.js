import { useContext, useState } from 'react';
import { ProductsDataDispatchContext } from '../../context/ProductsContext';
import ModalProducts from '../Modal/ModalProducts';

const ProductsComponent = () => {
  const dispatch = useContext(ProductsDataDispatchContext);
  const [showModalProducts, setShowModalProducts] = useState(false);

  const data = [
    {
      title: 'Headphone Type',
      dropdownItems: ['']
    },
    {
      title: 'Price',
      dropdownItems: ['$289', '$39', '$559', '$89']
    },
    {
      title: 'Material',
      dropdownItems: ['Cotton', 'Leather', 'Plastic']
    },
    {
      title: 'Color',
      dropdownItems: ['Show available colors']
    },
    {
      title: 'Review',
      dropdownItems: [1, 2, 3, 4, 5]
    },
    {
      title: 'Offer',
      dropdownItems: ['']
    },
    {
      title: 'All Filters',
      dropdownItems: ['']
    }
  ];

  return (
    <div className="products">
      <div className="nav-first-block">
        <nav className="nav-bar">
          <ul className="nav_links">
            {data.map((item, index) => (
              item.title !== 'Review' && 
              item.title !== 'Color' && 
              item.title !== 'Material' && 
              item.title !== 'Price'
              ? (
                <li key={index}>
                  <a href="/" className="Categoriesx">
                    {item.title} 
                  </a>
                  <ul className="dropdown">
                    {item.dropdownItems.map((dropdownItem, dropdownIndex) => (
                      <li key={dropdownIndex}>
                        <a href="/" className="Categories">
                          {dropdownItem} 
                        </a>
                      </li>
                    ))}
                  </ul>
                </li>
              ) : (
                <li key={index}>
                  <a href="/" className="Categoriesx">
                    {item.title} 
                  </a>
                  <ul className="dropdown">
                    {item.dropdownItems.map((dropdownItem, dropdownIndex) => (
                      <li key={dropdownIndex}>
                        <span
                          className={`Categories review`}
                          onClick={() => {
                            if (item.title === 'Review') {
                              dispatch({
                                type: 'FILTER_STARS',
                                value: dropdownItem
                              });
                            } else if  (item.title === 'Color') {
                              setShowModalProducts(true);
                            }
                             else if (item.title === 'Material') {
                              dispatch({
                                type: 'FILTER_MATERIAL',
                                value: dropdownItem
                              });
                            }
                             else if (item.title === 'Price') {
                              dispatch({
                                type: 'FILTER_PRICE',
                                value: dropdownItem
                              });
                            }
                          }}
                        >
                          {dropdownItem}
                        </span>
                      </li>
                    ))}
                  </ul>
                </li>
              )
            ))}
          </ul>
        </nav>
      </div>
      {showModalProducts && (
        <ModalProducts
          showModalProducts={showModalProducts}
          setShowModalProducts={setShowModalProducts}
        />
        )}
    </div>
  );
};

export default ProductsComponent;
