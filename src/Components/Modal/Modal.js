import React from 'react';
import './Modal.css';
import { faXmark } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';

const Modal = ({ showModal, setShowModal, product }) => {
  return (
    <>
      <div className='Overlay'>
        <div className='ContModal'>
          <div className='HeaderModal'>
            <h3 className='modalTitle'>Your Cart</h3>
            <button className='btn_cart'>View Cart</button>
            <Link to={`${process.env.PUBLIC_URL}/product/0`}>
            <button className='btn_cart'>Proceed with shopping</button>
            </Link>
          </div>
          <FontAwesomeIcon icon={faXmark} className='btn_close' 
          onClick={() => setShowModal(false)} />
          {product && (
            <div className="ProductImage">
                <div>
                  <img src={require(`../../images/${product.image}`)} 
                  alt="Product" className="headphoImg" />
                </div>
                <div>
                  <h3 className='modalTitle'>{product.type}</h3>
                  <p>{product.price}</p>
                </div>
            </div>
          )}
        </div>
      </div>
    </>
  );
};

export default Modal