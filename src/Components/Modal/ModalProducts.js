import './ModalProducts.css';
import { faXmark } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';


const ModalProducts = ({ showModalProducts, setShowModalProducts }) => {

  return (
    <>
    <div className='Overlay'>
      <div className='ContModalPrd'>
        <h1 className='colorTitle'>Choose your color</h1>
        <FontAwesomeIcon icon={faXmark} className='btn_close' 
          onClick={() => setShowModalProducts(false)} />

          <div className='colorContainer'>
          
            <span className='gradientContainer'>
          
                <div className='pink Color'>
                  <Link to={`${process.env.PUBLIC_URL}/product/9`} target="_blank" 
                  rel="noopener noreferrer" style={{ textDecoration: 'none' }}>
                  <p style={{ color: '#ebb1cf' }}>.</p>
                  </Link>
                </div>
                <div className='pink1 Color'>
                <Link to={`${process.env.PUBLIC_URL}/product/6`} target="_blank" 
                  rel="noopener noreferrer" style={{ textDecoration: 'none' }}>
                <p style={{ color: '#ed7ab5' }}>.</p>
                </Link>
                </div>
                <div className='pink2 Color'>
                <Link to={`${process.env.PUBLIC_URL}/product/11`} target="_blank" 
                  rel="noopener noreferrer" style={{ textDecoration: 'none' }}>
                <p style={{ color: '#ee4a9f' }}>.</p>
                </Link>
                </div>
                <div className='pink3 Color'>
                <Link to={`${process.env.PUBLIC_URL}/product/9`} target="_blank" 
                  rel="noopener noreferrer" style={{ textDecoration: 'none' }}>
                <p style={{ color: '#ee2b91' }}>.</p>
                </Link>
                </div>
            </span>    
           

            <span className='gradientContainer'>
                <div className='blue Color'>
                <Link to='http://localhost:3000/product/8' target="_blank" 
                  rel="noopener noreferrer" style={{ textDecoration: 'none' }}>
                <p style={{ color: '#a9a1ea' }}>.</p>
                </Link>
                </div>
                <div className='blue1 Color'>
                <Link to='http://localhost:3000/product/8' target="_blank" 
                  rel="noopener noreferrer" style={{ textDecoration: 'none' }}>
                <p style={{ color: '#7565ed' }}>.</p>
                </Link>
                </div>
                <div className='blue2 Color'>
                <Link to='http://localhost:3000/product/8' target="_blank" 
                  rel="noopener noreferrer" style={{ textDecoration: 'none' }}>
                <p style={{ color: '#5d4aed' }}>.</p>
                </Link>
                </div>
                <div className='blue3 Color'>
                <Link to='http://localhost:3000/product/8' target="_blank" 
                  rel="noopener noreferrer" style={{ textDecoration: 'none' }}>
                <p style={{ color: '#422bee' }}>.</p>
                </Link>
                </div>
            </span>      

            <span className='gradientContainer'>
                <div className='green Color'>
                <Link to={`${process.env.PUBLIC_URL}/product/3`} target="_blank" 
                  rel="noopener noreferrer" style={{ textDecoration: 'none' }}>
                <p style={{ color: '#a4eec3' }}>.</p>
                </Link>
                </div>
                <div className='green1 Color'>
                <Link to={`${process.env.PUBLIC_URL}/product/3`} target="_blank" 
                  rel="noopener noreferrer" style={{ textDecoration: 'none' }}>
                <p style={{ color: '#6aea9f' }}>.</p>
                </Link>
                </div>
                <div className='green2 Color'>
                <Link to={`${process.env.PUBLIC_URL}/product/3`} target="_blank" 
                  rel="noopener noreferrer" style={{ textDecoration: 'none' }}>
                <p style={{ color: '#39e781' }}>.</p>
                </Link>
                </div>
                <div className='green3 Color'>
                <Link to={`${process.env.PUBLIC_URL}/product/3`} target="_blank" 
                  rel="noopener noreferrer" style={{ textDecoration: 'none' }}>
                <p style={{ color: '#1fec74' }}>.</p>
                </Link>
                </div>
            </span>    

            <span className='gradientContainer'>
                <div className='white Color'>
                <Link to={`${process.env.PUBLIC_URL}/product/5`} target="_blank" 
                  rel="noopener noreferrer" style={{ textDecoration: 'none' }}>
                <p style={{ color: 'white' }}>.</p>
                </Link>
                </div>
                <div className='white1 Color'>
                <Link to={`${process.env.PUBLIC_URL}/product/7`} target="_blank" 
                  rel="noopener noreferrer" style={{ textDecoration: 'none' }}>
                <p style={{ color: 'rgb(206, 204, 204)' }}>.</p>
                </Link>
                </div>
                <div className='grey Color'>
                <Link to={`${process.env.PUBLIC_URL}/product/2`} target="_blank" 
                  rel="noopener noreferrer" style={{ textDecoration: 'none' }}>
                <p style={{ color: 'rgb(73, 71, 71)' }}>.</p>
                </Link>
                </div>
                <div className='black Color'>
                <Link to={`${process.env.PUBLIC_URL}/product/0`} target="_blank" 
                  rel="noopener noreferrer" style={{ textDecoration: 'none' }}>
                <p style={{ color: 'black' }}>.</p>
                </Link>
                </div>
            </span>  

          </div>
      </div>
    </div>
  </>
  )
}

export default ModalProducts



