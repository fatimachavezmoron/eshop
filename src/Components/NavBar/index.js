import logo from '../../images/logo.png';
import { EOS_PERSON, EOS_SHOPPING_CART, EOS_SEARCH } from 'eos-icons-react';
import { faBars} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link } from 'react-router-dom';

const NavBar = () => {

  return (
  <header id="header">
    <nav>
      <span className='checkbtn' >
      <FontAwesomeIcon icon={faBars} className='barsIcon'/>
      </span>
    </nav>
      <Link to={`${process.env.PUBLIC_URL}/`} className="nav-link-logo">
        <img className='logo' src={logo} alt='logo'/>
      </Link>
      <nav className="nav-bar">
          <ul className="nav_links">
            <li>
              <a href={`${process.env.PUBLIC_URL}/product/0`} className="active Categories">
                Categories 
              </a>  
            </li>
            <li>
              <a href={`${process.env.PUBLIC_URL}/product/0`}  className="nav-link">
              Deals
              </a>
            </li>
            <li> 
              <a href={`${process.env.PUBLIC_URL}/product/0`}  className="nav-link">
               What's New
              </a>
            </li> 
            <li>  
              <a href={`${process.env.PUBLIC_URL}/product/0`}  className="nav-link">
                Delivery
              </a>
            </li>
          </ul>
      </nav>
        <div class="search-container">
          <input className="search" type="text" id="search-input" placeholder="Search Products..." />
          <a href="./products/product.html" className="btn btn-no-border">
            <EOS_SEARCH className='eos-icons-search'/>
          </a>
        </div>
        <nav class="nav-icons">
          <ul class="nav_links">
            <li>
            <Link to={`${process.env.PUBLIC_URL}/register`} className="nav-link-icon">
              <EOS_PERSON className="eos-icons" />
            </Link>
            </li>
            <li>
                <Link to={`${process.env.PUBLIC_URL}/paypal`} className="nav-link-icon">
                <EOS_SHOPPING_CART className='eos-icons'/>
                </Link>
            </li>
          </ul>         
        </nav>
  </header>    
  );
}

export default NavBar